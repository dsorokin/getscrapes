var mongoose = glob.modules.mongoose;

var bill = new mongoose.Schema({
  type:             { type: "string" },
  login:            { type: "string" },
  password:         { type: "string" },
  frequency:        { type: "string", "enum": ["weekly", "monthly", "quarterly", "annually", "other"]},
  amount:           { type: "number" },
  comments:         { type: "string" },
  contractName:     { type: "string" },
  supplier:         { type: "string" },
  paysSupplier:     { type: "string" }, //sharedbill
  house:            { type: "string" },
  userId:           { type: "string" }, //personal bill
  isShared:         { type: "bool" },
  billCredit:       { type: "number", default: 0 },
  isPaid:           { type: "bool", default: false },
  isDebt:           { type: "bool", default: false },
  creaditPayType:   { type: "string", "enum": ["debit", "after"] },
  classPayType:     { type: "string", "enum": ["online", "offline", "oneoff"] },
  sharedIds:        { type: "array", items: [ { type: "object" } ] }, //sharedIds array of object:{id,value,type,isPaid,credit}
  paymentDate:      { type: "date" },
  changeAmount:     { type: "array" },
  dt:               { type: "date", default: Date.now()},
  lm:               { type: "date", default: Date.now()}
});

bill.virtual('entity').get(function () {
  var shared = []
  var entity = {
    id: this._id.toString(),
    type: this.type,
    login: this.login,
    frequency: this.frequency,
    amount: this.amount,
    changeAmount: this.changeAmount,
    house: this.house,
    contractName: this.contractName,
    supplier: this.supplier,
    comments: this.comments,
    paysSupplier: this.paysSupplier,
    userId: this.userId,
    isShared: this.isShared,
    isPaid: this.isPaid,
    creaditPayType: this.creaditPayType,
    classPayType: this.classPayType,
    sharedIds: this.sharedIds,
    paymentDate: this.paymentDate,
    dt: this.dt,
    lm: this.lm
  };
  if (this.sharedIds[0]){
      for (i=0; i < this.sharedIds ; i++ ){
          shared.push(this.sharedIds[i])    
      }
  }

  return entity;
});

module.exports = mongoose.model('bill', bill);
