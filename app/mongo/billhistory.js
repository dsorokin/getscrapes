var mongoose = glob.modules.mongoose;

var billhistory = new mongoose.Schema({
  billId:         { type: "string" },
  userId:         { type: "string" },

  balance:        { type: "string" },
  nextbillDate:   { type: "string" },
  monthlyChargeValue:   { type: "string" },
  billDate:             { type: "string" },
  paymentDueDate:   { type: "string" },
  latestBillAmount:   { type: "string" },
  lengthOfContractValue:   { type: "string" },
  contractRenewalDateValue:   { type: "string" },
  tariffNameValue:   { type: "string" },

  dt:             { type: "date" },
  lm:             { type: "date" }
});

billhistory.virtual('entity').get(function () {
  var entity = {
    id: this._id.toString(),
    billId: this.billId,
    userId: this.userId,
    nextbillDate: this.nextbillDate,
    monthlyChargeValue: this.monthlyChargeValue,
    billDate: this.billDate,
    paymentDueDate: this.paymentDueDate,
    latestBillAmount: this.latestBillAmount,
    lengthOfContractValue: this.lengthOfContractValue,
    contractRenewalDateValue: this.contractRenewalDateValue,
    tariffNameValue: this.tariffNameValue,
    dt: this.dt,
    lm: this.lm
  };
  return entity;
});

module.exports = mongoose.model('billhistory', billhistory);
