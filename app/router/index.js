module.exports = function (app) {

  app.get('/', function (req, res) {
    res.render('index.jade', { title: 'GetScrapes' });
  });
  require('./savescrapes')(app);
};
