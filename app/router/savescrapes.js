var create =  function(req, res) {
    var data = req.query;
    if(data && typeof(data.userId) != 'undefined' && typeof(data.nextbillDate) != 'undefined' && typeof(data.billId) != 'undefined') {
        var billData = {};
        billData['paymentDate'] = glob.modules.moment(data.nextbillDate).format('DD/MM/YYYY');
        mongo.bill.update({ _id: data.billId, userId: data.userId }, billData, function (err, bill) {
            if(!err) {
                if(bill) {
                    res.send ({
                        code: 200,
                        data: bill
                    });
                } else {
                    res.send({
                        code: 400,
                        error: 'bill update error'
                    });
                }
            } else {
                res.send({
                    code: 400,
                    error: 'bill update error'
                });
            }
        });
    } else {
        res.send ({
            code: 400,
            error: 'body not found'
        })
    }
}

var update =  function(req, res) {
  var data = req.query;
  var billData = {};
  if(typeof(data.monthlyChargeValue) != 'undefined') {
    billData['amount'] = parseFloat(data.monthlyChargeValue);
  }
  if(typeof(data.billCredit) != 'undefined') {
    billData['billCredit'] = parseFloat(data.billCredit);
  }
  if (data && typeof(data.userId) != 'undefined' && typeof(data.billId) != 'undefined') {
      mongo.bill.update({ _id: data.billId, userId: data.userId }, billData, function (err, count) {
        if (!err) {
          if(count>0){
            res.send ({
              code: 200,
              message: 'updated billhistory'
            });
          }else{
            res.send ({
                code: 400,
                error: 'nothing to update billhistory'
            })
          }
        }else{
          res.send({
            code: 400,
            error: 'data find error billhistory'
          });
        }
      });
  }else{
    res.send ({
      code: 400,
      error: 'params not found'
    })
  }
}

// parse input data (HTML table)
var parse =  function(req, res) {
  var data = req.query;
  if (data && typeof(data.el) != 'undefined' && typeof(data.billId) != 'undefined' && typeof(data.userId) != 'undefined') {
    var jsReg = /<td>([\s\S]*?)<\/td>/gim;
    var html = data.el;
    html = html.replace(/\n/g,"");
    html = html.replace(/ /g,"");

    var myArray = [];
    var msg = [];
    while ((myArray = jsReg.exec(html)) != null) {
      msg.push(myArray[1]);
    }

    if(msg && msg[1] && msg[5]) {
      data["billDate"] = msg[1];
      if(msg[5].indexOf('£') !== -1) {
          msg[5] = msg[5].replace('£', '');
          msg[5] = parseFloat(msg[5]);
      }
      data["balance"] = msg[5];
      delete data.el;
    }

    mongo.billhistory.create(data, function (err, billhistory) {
        if(!err) {
            if(billhistory) {
                res.send ({
                    code: 200,
                    data: billhistory
                });
            } else {
                res.send({
                    code: 400,
                    error: 'billhistory create error'
                });
            }
        } else {
            res.send({
                code: 400,
                error: 'billhistory create error'
            });
        }
    });

  }else{
    res.send ({
      code: 400,
      error: 'params not found'
    })
  }
}

module.exports = function (app) {
    app.post('/savescrapes', create);
    app.post('/updatescrapes', update);
    app.post('/parse', parse);
};
