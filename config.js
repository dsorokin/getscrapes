var url = require('url');
module.exports = function (env) {
  var config;
  config = {
      app: {
          url: 'http://localhost:5000',
          name: 'GetScrapes',
          port: 5000,
          secretString: 'secretString',
          maxAge: 3*60*60*1000
      },
      log: {
          level: 'info',
          path: __dirname+'/server.log'
      },
      db: {
          mongoUrl: 'mongodb://heroku:71eb5116ad068bd32aa9fab0b215faa9@alex.mongohq.com:10062/app10352788', // for heroku
          // mongoUrl: process.env.MONGOHQ_URL || 'mongodb://localhost/household', // for localhost
      }
  };
  switch (env) {
    case 'production': {
      // config.app.url = 'http://localhost:5000'; // for localhost
      config.app.url = 'http://getscrapes.herokuapp.com'; // for heroku
      break;
    }
    case 'development': {
      config.db.mongoUrl = 'mongodb://heroku:71eb5116ad068bd32aa9fab0b215faa9@alex.mongohq.com:10062/app10352788'; // for heroku
      // config.db.mongoUrl = process.env.MONGOHQ_URL || 'mongodb://localhost/household'; // for localhost
      break;
    }
    default: {
      break;
    }
  }

  return config;
};

