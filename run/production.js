var server = require('../app/server');
var build = require('./build');
var port = process.env.PORT || 5002;
build.start(function () {
  server.start(port, 'production');
});
