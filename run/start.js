var root = __dirname + '/..';

var showcase = require('Showcase');
showcase.start({
  coffee: false,
  type: 'server',
  port: 5500,
  root: __dirname,
  lint: [
    '--nomen',
    '--sloppy',
    '--indent', '2',
    '--color',
    '--node', 'true',
    '--predef', 'glob',
    '--predef', 'request'
  ],
  watch: {
    dirs: [
      root + '/app/mongo',
      root + '/app/router',
      root + '/run'
    ],
    files: [
      root + '/app/server.js',
      root + '/app/utils.js',
      root + '/config.js'
    ]
  },
  path: {
    runner: {
      server: __dirname + '/development.js',
    }
  }
});
